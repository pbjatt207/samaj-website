<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="imgs/logo.png" type="image/x-icon"/>
    	<title>Shreemali Brahmin Samaj</title>

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="icomoon/style.css">
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
    </head>
    <body class="homepage">
        <div id="home-section" class="bannerBg">
            <div class="overlay">
                <header>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4">
                                <img src="imgs/logo.png" alt="" class="logo"> SHREEMALI BRAHMIN SAMAJ
                            </div>
                            <div class="col-sm-8">
                                <nav class="main-navbar text-right">
                                    <ul class="mr-auto d-inline-flex">
                                        <li class="active"><a href="#home-section" rel="slide">Home</a></li>
                                        <li><a href="#about-section" rel="slide">VIP</a></li>
                                        <li><a href="#about-section" rel="slide">Samaj Places</a></li>
                                        <li><a href="#about-section" rel="slide">News</a></li>
                                        <li><a href="#event-section" rel="slide">Events</a></li>
                                        <li><a href="#moment-section" rel="slide">Become Our Memeber</a></li>
                                        <li><a href="#rsvp-section" rel="slide">Login</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </header>
                <section id="banner-search" class="text-center">
                    <h3>Discover Peoples Around You</h3>
                    <p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."<br>"There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."</p>
                    <form method="post">
                        <div class="input-group banner-search-form">
                            <div class="input-group-prepend">
                                <label for="search_city" class="input-group-text">
                                    <i class="icon-pin_drop"></i>
                                </label>
                            </div>
                            <select name="" value="" class="form-control" id="search_city">
                                <option value="">Your City</option>
                                <option value="">Jodhpur</option>
                            </select>
                            <div class="input-group-prepend">
                                <label for="search_keyword" class="input-group-text bl">
                                    <i class="icon-search"></i>
                                </label>
                            </div>
                            <input type="text" name="" value="" class="form-control" id="search_keyword" placeholder="Search Keywords...">
                            <button type="button" class="btn btn-primary"> Search</button>
                        </div>
                    </form>
                </section>
                <input type="hidden" id="wedding_date" value="2020-02-09">

            </div>
            <div id="demo" class="carousel slide carousel-fade" data-ride="carousel">
                <!-- The slideshow -->
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="imgs/banner_bg.jpg" alt="Los Angeles">
                    </div>
                    <div class="carousel-item">
                        <img src="imgs/banner_bg.jpg" alt="Chicago">
                    </div>
                    <div class="carousel-item">
                        <img src="imgs/banner_bg.jpg" alt="New York">
                    </div>
                </div>
            </div>
        </div>

        <section id="about-section" class="home-section">
            <div class="container">
                <h3 class="section-title">Our <span>VIP</span></h3>
                <p class="text-center">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."<br>"There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."</p>

                <div class="owl-carousel">
                    <div class="carousel-section">
                        <div class="">
                            <img src="imgs/man.jpg" alt="">
                        </div>
                        <div class="carousel-section-body">
                            <h4>Joy Due</h4>
                            <div><i class="icon-user2"></i> President - Mahamandir</div>
                            <div><i class="icon-mobile2"></i> +91 9636XXXXXX</div>
                            <div><i class="icon-mail_outline"></i> joy.due@hotmail.com</div>
                        </div>
                    </div>
                    <div class="carousel-section">
                        <div class="">
                            <img src="imgs/girl.jpg" alt="">
                        </div>
                        <div class="carousel-section-body">
                            <h4>Alina Jam</h4>
                            <div><i class="icon-user2"></i> Vice President - Mahamandir</div>
                            <div><i class="icon-mobile2"></i> +91 9636XXXXXX</div>
                            <div><i class="icon-mail_outline"></i> alina.jam@example.com</div>
                        </div>
                    </div>
                    <div class="carousel-section">
                        <div class="">
                            <img src="imgs/man.jpg" alt="">
                        </div>
                        <div class="carousel-section-body">
                            <h4>Joy Due</h4>
                            <div><i class="icon-user2"></i> President - Mahamandir</div>
                            <div><i class="icon-mobile2"></i> +91 9636XXXXXX</div>
                            <div><i class="icon-mail_outline"></i> joy.due@hotmail.com</div>
                        </div>
                    </div>
                    <div class="carousel-section">
                        <div class="">
                            <img src="imgs/girl.jpg" alt="">
                        </div>
                        <div class="carousel-section-body">
                            <h4>Alina Jam</h4>
                            <div><i class="icon-user2"></i> Vice President - Mahamandir</div>
                            <div><i class="icon-mobile2"></i> +91 9636XXXXXX</div>
                            <div><i class="icon-mail_outline"></i> alina.jam@example.com</div>
                        </div>
                    </div>
                    <div class="carousel-section">
                        <div class="">
                            <img src="imgs/man.jpg" alt="">
                        </div>
                        <div class="carousel-section-body">
                            <h4>Joy Due</h4>
                            <div><i class="icon-user2"></i> President - Mahamandir</div>
                            <div><i class="icon-mobile2"></i> +91 9636XXXXXX</div>
                            <div><i class="icon-mail_outline"></i> joy.due@hotmail.com</div>
                        </div>
                    </div>
                    <div class="carousel-section">
                        <div class="">
                            <img src="imgs/girl.jpg" alt="">
                        </div>
                        <div class="carousel-section-body">
                            <h4>Alina Jam</h4>
                            <div><i class="icon-user2"></i> Vice President - Mahamandir</div>
                            <div><i class="icon-mobile2"></i> +91 9636XXXXXX</div>
                            <div><i class="icon-mail_outline"></i> alina.jam@example.com</div>
                        </div>
                    </div>
                </div>

                <div class="landscape-ad ads text-center">
                    <img src="imgs/3by1.jpg" alt="">
                </div>
            </div>
        </section>

        <section id="about-section" class="home-section gray_bg">
            <div class="container">
                <h3 class="section-title">Samaj <span>Places</span></h3>
                <p class="text-center">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."<br>"There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."</p>

                <div class="owl-carousel"><?php
                    for($i = 1; $i <= 10; $i++) :
                    ?><div class="carousel-section">
                        <div class="">
                            <img src="imgs/place.jpg" alt="">
                        </div>
                        <div class="carousel-section-body">
                            <h4>Samaj Bhawan Jodhpur</h4>
                            <div class="mb-2"><i class="icon-pin_drop"></i> Mahamandir, Jodhpur 342001</div>
                            <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, ...</p>
                            <div class="row">
                                <div class="col">
                                    <a href="#" class="btn btn-sm btn-block btn-outline-primary"><i class="icon-map-marker"></i> Map</a>
                                </div>
                                <div class="col">
                                    <a href="#" class="btn btn-sm btn-block btn-primary">Details <i class="icon-long-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div><?php
                    if($i%3 == 0) {
                    ?><div class="carousel-section">
                        <div class="ads">
                            <img src="imgs/1by2_ad.jpg" alt="">
                        </div>
                    </div><?php
                    }
                    endfor;
                ?></div>
            </div>
        </section>

        <section class="home-section left-title">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h3 class="section-title">Upcoming <span>Events</span></h3>
                        <p class="text-justify">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p><?php

                        for($i = 1; $i <= 4; $i++) :
                        ?><div class="event-section mb-4">
                            <div class="row">
                                <div class="col-2 event-date">
                                    <div class="date">14</div>
                                    <div class="">Feb 20</div>
                                </div>
                                <div class="col-10">
                                    <div class="event-content">
                                        <h4>Lorem Ipsum</h4>
                                        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                        <a href="#" class="btn btn-primary btn-sm">View More &raquo;</a>
                                    </div>
                                </div>
                            </div>
                        </div><?php
                        endfor;
                    ?></div>
                    <div class="col-sm-6">
                        <h3 class="section-title">Latest <span>News</span></h3>
                        <p class="text-justify">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p><?php

                        for($i = 1; $i <= 3; $i++) :
                        ?><div class="news-section mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <div class="news-image">
                                        <img src="imgs/place.jpg" alt="">
                                        <div class="news-date">
                                            <div class="date">14</div>
                                            <div class="">Feb 20</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="news-content">
                                        <h4>Lorem Ipsum</h4>
                                        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam...</p>
                                        <a href="#" class="btn btn-primary btn-sm">View More &raquo;</a>
                                    </div>
                                </div>
                            </div>
                        </div><?php
                        endfor;
                    ?></div>
                </div>
            </div>
        </section>

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h4>Quick Links</h4>
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">News</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">VIP</a></li>
                            <li><a href="#">Samaj Places</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h4>Terms &amp; Policies</h4>
                        <ul>
                            <li><a href="#">Terms &amp; Conditions</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Instructions</a></li>
                            <li><a href="#">Frequently Asked Questions</a></li>
                            <li><a href="#">Help</a></li>
                            <li><a href="#">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h4>Useful Links</h4>
                        <ul>
                            <li><a href="#">Login</a></li>
                            <li><a href="#">My Profile</a></li>
                            <li><a href="#">Become Our Member</a></li>
                            <li><a href="#">Matrimony</a></li>
                            <li><a href="#">Directory</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h4>Subscribe Us</h4>
                        <p>Subscribe us to get updated to latest news.</p>
                        <div class="input-group">
                            <input type="text" name="" value="" class="form-control" placeholder="Enter Your Email">
                            <div class="input-group-append">
                                <button type="button" class="btn btn-outline-primary">Send</button>
                            </div>
                        </div>
                        <div class="mt-2 social-links">
                            <a href="#"> <i class="icon-facebook"></i> </a>
                            <a href="#"> <i class="icon-twitter"></i> </a>
                            <a href="#"> <i class="icon-linkedin2"></i> </a>
                            <a href="#"> <i class="icon-instagram"></i> </a>
                            <a href="#"> <i class="icon-whatsapp"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <script src="js/jquery.min.js" type="text/javascript"></script>
        <script src="js/validation.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/owl.carousel.min.js" type="text/javascript"></script>
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script src="js/main.js" type="text/javascript"></script>
    </body>
</html>
