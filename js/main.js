$(function() {
    AOS.init();

    $('.owl-carousel').owlCarousel({
        loop:true,
        margin: 10,
        responsiveClass: true,
        navText: ['<i class="icon-angle-left"></i>', '<i class="icon-angle-right"></i>'],
        responsive: {
            0:{
                items:1,
                nav:true
            },
            600:{
                items:3,
                nav:false
            },
            1000:{
                items: 4,
                nav: true,
                loop: false
            }
        }
    });

    $(window).scroll(function() {
        // console.log("Scroll Top: "+ $(window).scrollTop());
        // console.log("Window Height: "+ $(window).height());
        // console.log("Document Height: "+ $(document).height());
        // console.log("RSVP: "+ $('#rsvp-section').offset().top);
        if($(window).scrollTop() >= 150) {
            $("header").addClass("bg-dark");
        } else {
            $("header").removeClass("bg-dark");
        }
    });

    $(document).on('click', '[rel=slide]', function(e) {
        e.preventDefault();
        var sel = $(this).attr('href');
        $("html, body").animate({ scrollTop: $(sel).offset().top - 80 }, 1000);
    });
});
